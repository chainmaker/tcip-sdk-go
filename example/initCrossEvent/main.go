/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"fmt"
	"os"
	"path"
	"time"

	"chainmaker.org/chainmaker/common/v2/log"
	"chainmaker.org/chainmaker/tcip-go/v2/common"
	"chainmaker.org/chainmaker/tcip-go/v2/common/cross_chain"
	tcip_sdk_go "chainmaker.org/chainmaker/tcip-sdk-go/v2"
	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
)

var (
	crossChainGatewayConfig1 = &request.ClientConfig{
		Address:        "127.0.0.1:19996",
		TlsEnable:      true,
		CaPath:         "../testFile/certs/ca.crt",
		ClientCertPath: "../testFile/certs/client.crt",
		ClientKeyPath:  "../testFile/certs/client.key",
		Timeout:        1000,
		AccessCode:     "testAccessCode",
	}
	//crossChainGatewayConfig2 = &request.ClientConfig{
	//	Address: "127.0.0.1:19997",
	//}
	relayGatewayConfig = &request.ClientConfig{
		Address:        "127.0.0.1:19999",
		TlsEnable:      true,
		CaPath:         "../testFile/certs/ca.crt",
		ClientCertPath: "../testFile/certs/client.crt",
		ClientKeyPath:  "../testFile/certs/client.key",
		Timeout:        1000,
		AccessCode:     "testAccessCode",
	}
)

func main() {
	logger, _ := log.InitSugarLogger(&log.LogConfig{
		Module:       "[TCIP-SDK]",
		LogPath:      path.Join(os.TempDir(), time.Now().String()),
		LogLevel:     log.LEVEL_DEBUG,
		MaxAge:       300,
		JsonFormat:   false,
		ShowLine:     true,
		LogInConsole: true,
		RotationSize: 10,
	})
	crossChainGatewayConfig1.Log = logger
	//crossChainGatewayConfig2.Log = logger
	relayGatewayConfig.Log = logger
	tcipSdk, err := tcip_sdk_go.NewTcipSdk(crossChainGatewayConfig1, relayGatewayConfig)
	if err != nil {
		panic(err)
	}
	crossChainGateway1 := tcipSdk.CrossChainGateway
	relayGateway := tcipSdk.RelayGateway

	//crossChainGateway2, err := crossChainGateway.NewCrossChainGateway(crossChainGatewayConfig2)
	//if err != nil {
	//	panic(err)
	//}

	fmt.Println("**********init cross chain1 event**********")
	res, err := crossChainGateway1.CrossChainEvent(&cross_chain.CrossChainEventRequest{
		Version: common.Version_V1_0_0,
		Operate: common.Operate_SAVE,
		CrossChainEvent: &common.NewCrossChain{
			DestGatewayId:       "MAIN_GATEWAY_ID-relay-1",
			DestChainRid:        "main chain",
			DestContractName:    "crossChainSaveQuery",
			DestTryMethod:       "query",
			SrcCancelMethod:     "saveState",
			SrcConfirmMethod:    "save",
			CrossId:             "4",
			ConfigConstractName: "crossChainContract",
			Desc:                "cross chain query, son to main",
			TriggerCrossType:    0,
			DestAbi:             "",
			SrcAbi:              "",
			SrcChainRid:         "chainmaker001",
			SrcGatewayId:        "0",
			SrcContractName:     "crossChainSaveQuery",
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())
	res, err = crossChainGateway1.CrossChainEvent(&cross_chain.CrossChainEventRequest{
		Version: common.Version_V1_0_0,
		Operate: common.Operate_SAVE,
		CrossChainEvent: &common.NewCrossChain{
			DestGatewayId:       "1",
			DestChainRid:        "chainmaker002",
			DestContractName:    "crossChainSaveQuery",
			DestTryMethod:       "query",
			SrcCancelMethod:     "saveState",
			SrcConfirmMethod:    "save",
			CrossId:             "2",
			ConfigConstractName: "crossChainContract",
			Desc:                "cross chain query, son to son",
			TriggerCrossType:    0,
			DestAbi:             "",
			SrcAbi:              "",
			SrcChainRid:         "chainmaker001",
			SrcGatewayId:        "0",
			SrcContractName:     "crossChainSaveQuery",
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())
	res, err = crossChainGateway1.CrossChainEvent(&cross_chain.CrossChainEventRequest{
		Version: common.Version_V1_0_0,
		Operate: common.Operate_SAVE,
		CrossChainEvent: &common.NewCrossChain{
			DestGatewayId:       "MAIN_GATEWAY_ID-relay-1",
			DestChainRid:        "main chain",
			DestContractName:    "crossChainSaveQuery",
			DestTryMethod:       "save",
			DestCancelMethod:    "saveState",
			DestConfirmMethod:   "saveState",
			SrcCancelMethod:     "saveState",
			SrcConfirmMethod:    "saveState",
			CrossId:             "3",
			ConfigConstractName: "crossChainContract",
			Desc:                "cross chain save, son to main",
			TriggerCrossType:    1,
			DestAbi:             "",
			SrcAbi:              "",
			SrcChainRid:         "chainmaker001",
			SrcGatewayId:        "0",
			SrcContractName:     "crossChainSaveQuery",
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())
	res, err = crossChainGateway1.CrossChainEvent(&cross_chain.CrossChainEventRequest{
		Version: common.Version_V1_0_0,
		Operate: common.Operate_SAVE,
		CrossChainEvent: &common.NewCrossChain{
			DestGatewayId:       "1",
			DestChainRid:        "chainmaker002",
			DestContractName:    "crossChainSaveQuery",
			DestTryMethod:       "save",
			DestCancelMethod:    "saveState",
			DestConfirmMethod:   "saveState",
			SrcCancelMethod:     "saveState",
			SrcConfirmMethod:    "saveState",
			CrossId:             "1",
			ConfigConstractName: "crossChainContract",
			Desc:                "cross chain save, son to son",
			TriggerCrossType:    1,
			DestAbi:             "",
			SrcAbi:              "",
			SrcChainRid:         "chainmaker001",
			SrcGatewayId:        "0",
			SrcContractName:     "crossChainSaveQuery",
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())
	res, err = crossChainGateway1.CrossChainEvent(&cross_chain.CrossChainEventRequest{
		Version: common.Version_V1_0_0,
		Operate: common.Operate_SAVE,
		CrossChainEvent: &common.NewCrossChain{
			DestGatewayId:       "1",
			DestChainRid:        "chainmaker002",
			DestContractName:    "erc20_02",
			DestTryMethod:       "minter",
			DestCancelMethod:    "burn",
			DestConfirmMethod:   "transfer",
			SrcCancelMethod:     "minter",
			SrcConfirmMethod:    "",
			CrossId:             "0",
			ConfigConstractName: "crossChainContract",
			Desc:                "erc20",
			TriggerCrossType:    1,
			DestAbi:             "",
			SrcAbi:              "",
			SrcChainRid:         "chainmaker001",
			SrcGatewayId:        "0",
			SrcContractName:     "erc20_01",
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())

	fmt.Println("**********init main chain event**********")
	res, err = relayGateway.CrossChainEvent(&cross_chain.CrossChainEventRequest{
		Version: common.Version_V1_0_0,
		Operate: common.Operate_SAVE,
		CrossChainEvent: &common.NewCrossChain{
			DestGatewayId:       "0",
			DestChainRid:        "chainmaker001",
			DestContractName:    "crossChainSaveQuery",
			DestTryMethod:       "query",
			SrcCancelMethod:     "saveState",
			SrcConfirmMethod:    "save",
			CrossId:             "6",
			ConfigConstractName: "crossChainContract",
			Desc:                "cross chain query, main to son",
			TriggerCrossType:    0,
			DestAbi:             "",
			SrcAbi:              "",
			SrcChainRid:         "main chain",
			SrcGatewayId:        "MAIN_GATEWAY_ID-relay-1",
			SrcContractName:     "crossChainSaveQuery",
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())
	res, err = relayGateway.CrossChainEvent(&cross_chain.CrossChainEventRequest{
		Version: common.Version_V1_0_0,
		Operate: common.Operate_SAVE,
		CrossChainEvent: &common.NewCrossChain{
			DestGatewayId:       "0",
			DestChainRid:        "chainmaker001",
			DestContractName:    "crossChainSaveQuery",
			DestTryMethod:       "save",
			DestCancelMethod:    "saveState",
			DestConfirmMethod:   "saveState",
			SrcCancelMethod:     "saveState",
			SrcConfirmMethod:    "saveState",
			CrossId:             "5",
			ConfigConstractName: "crossChainContract",
			Desc:                "cross chain save, main to son",
			TriggerCrossType:    1,
			DestAbi:             "",
			SrcAbi:              "",
			SrcChainRid:         "main chain",
			SrcGatewayId:        "MAIN_GATEWAY_ID-relay-1",
			SrcContractName:     "crossChainSaveQuery",
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())
}
