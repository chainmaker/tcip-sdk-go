/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"chainmaker.org/chainmaker/common/v2/log"
	"chainmaker.org/chainmaker/tcip-go/v2/common"
	"chainmaker.org/chainmaker/tcip-go/v2/common/relay_chain"
	tcip_sdk_go "chainmaker.org/chainmaker/tcip-sdk-go/v2"
	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
	"fmt"
	"os"
	"path"
	"time"
)

var (
	relayGatewayConfig = &request.ClientConfig{
		Address:        "127.0.0.1:19999",
		ServerName:     "chainmaker.org",
		TlsEnable:      true,
		CaPath:         "../testFile/certs/ca.crt",
		ClientCertPath: "../testFile/certs/client.crt",
		ClientKeyPath:  "../testFile/certs/client.key",
		Timeout:        1000,
		AccessCode:     "testAccessCode",
	}
)

func main() {
	logger, _ := log.InitSugarLogger(&log.LogConfig{
		Module:       "[TCIP-SDK]",
		LogPath:      path.Join(os.TempDir(), time.Now().String()),
		LogLevel:     log.LEVEL_DEBUG,
		MaxAge:       300,
		JsonFormat:   false,
		ShowLine:     true,
		LogInConsole: true,
		RotationSize: 10,
	})
	relayGatewayConfig.Log = logger
	tcipSdk, err := tcip_sdk_go.NewTcipSdk(nil, relayGatewayConfig)
	if err != nil {
		panic(err)
	}
	relayGateway := tcipSdk.RelayGateway
	res, err := relayGateway.QueryCrossChain(&relay_chain.QueryCrossChainRequest{
		Version:    common.Version_V1_0_0,
		PageSize:   10,
		PageNumber: 1,
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.String())
}
