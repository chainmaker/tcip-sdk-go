package request

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitGRPCConnect(t *testing.T) {
	clientConfig := &ClientConfig{
		Address:   "127.0.0.1:19996",
		TlsEnable: false,
	}
	_, err := InitGRPCConnect(clientConfig)
	assert.Nil(t, err)

	clientConfig.TlsEnable = true
	_, err = InitGRPCConnect(clientConfig)
	assert.NotNil(t, err)

	clientConfig.CaPath = "../example/testFile/certs/ca.crt"
	_, err = InitGRPCConnect(clientConfig)
	assert.NotNil(t, err)

	clientConfig.ClientCertPath = "../example/testFile/certs/client.crt"
	_, err = InitGRPCConnect(clientConfig)
	assert.NotNil(t, err)

	clientConfig.ClientKeyPath = "../example/testFile/certs/client.key"
	_, err = InitGRPCConnect(clientConfig)
	assert.Nil(t, err)
}
