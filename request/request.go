/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

// Package request 请求工具
package request

import (
	//nolint
	"io/ioutil"
	"time"

	"chainmaker.org/chainmaker/common/v2/ca"
	"chainmaker.org/chainmaker/common/v2/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

// ClientConfig 客户端配置
type ClientConfig struct {
	Address        string
	TlsEnable      bool
	CaCert         []byte
	CaPath         string
	ClientCert     []byte
	ClientCertPath string
	ClientKey      []byte
	ClientKeyPath  string
	Timeout        int64
	ServerName     string
	AccessCode     string
	Log            log.LoggerInterface
}

// InitGRPCConnect  初始化grpc链接
//
//	@param clientConfig
//	@return *grpc.ClientConn
//	@return error
func InitGRPCConnect(clientConfig *ClientConfig) (*grpc.ClientConn, error) {
	var kacp = keepalive.ClientParameters{
		Time:                10 * time.Second,
		Timeout:             time.Second,
		PermitWithoutStream: true,
	}
	var tlsClient ca.CAClient
	if clientConfig.TlsEnable {
		var (
			err error
		)
		if clientConfig.CaCert == nil {
			clientConfig.CaCert, err = ioutil.ReadFile(clientConfig.CaPath)
			if err != nil {
				return nil, err
			}
		}
		if clientConfig.ClientCert == nil {
			clientConfig.ClientCert, err = ioutil.ReadFile(clientConfig.ClientCertPath)
			if err != nil {
				return nil, err
			}
		}
		if clientConfig.ClientKey == nil {
			clientConfig.ClientKey, err = ioutil.ReadFile(clientConfig.ClientKeyPath)
			if err != nil {
				return nil, err
			}
		}
		tlsClient = ca.CAClient{
			ServerName: clientConfig.ServerName,
			CaCerts:    []string{string(clientConfig.CaCert)},
			CertBytes:  clientConfig.ClientCert,
			KeyBytes:   clientConfig.ClientKey,
			Logger:     clientConfig.Log,
		}

		c, err := tlsClient.GetCredentialsByCA()
		if err != nil {
			return nil, err
		}
		return grpc.Dial(
			clientConfig.Address,
			grpc.WithTransportCredentials(*c),
			grpc.WithDefaultCallOptions(
				grpc.MaxCallRecvMsgSize(1024*1024),
				grpc.MaxCallSendMsgSize(1024*1024),
			),
			grpc.WithKeepaliveParams(kacp),
		)
	}
	return grpc.Dial(
		clientConfig.Address,
		grpc.WithInsecure(),
		grpc.WithDefaultCallOptions(
			grpc.MaxCallRecvMsgSize(1024*1024),
			grpc.MaxCallSendMsgSize(1024*1024),
		),
		grpc.WithKeepaliveParams(kacp),
	)
}
