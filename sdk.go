/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

// Package tcip_sdk_go sdk
package tcip_sdk_go

import (
	"errors"

	"chainmaker.org/chainmaker/tcip-sdk-go/v2/crossChainGateway"
	"chainmaker.org/chainmaker/tcip-sdk-go/v2/relayGateway"
	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
)

// TcipSdk SDK
type TcipSdk struct {
	CrossChainGateway crossChainGateway.CrossChainGateway
	RelayGateway      relayGateway.RelayGateway
}

// NewTcipSdk  新建sdk
//
//	@param crossChainGatewayConfig
//	@param relayGatewayConfig
//	@return *TcipSdk
//	@return error
func NewTcipSdk(crossChainGatewayConfig, relayGatewayConfig *request.ClientConfig) (*TcipSdk, error) {
	if crossChainGatewayConfig == nil && relayGatewayConfig == nil {
		return nil, errors.New("both crossChainGatewayConfig and relayGatewayConfig cannot be nil")
	}
	var err error
	tcipSdk := &TcipSdk{}
	if crossChainGatewayConfig != nil {
		tcipSdk.CrossChainGateway, err = crossChainGateway.NewCrossChainGateway(crossChainGatewayConfig)
		if err != nil {
			return nil, err
		}
	}
	if relayGatewayConfig != nil {
		tcipSdk.RelayGateway, err = relayGateway.NewRelayGateway(relayGatewayConfig)
		if err != nil {
			return nil, err
		}
	}
	return tcipSdk, nil
}
