/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

// Package relayGateway 中继网关
package relayGateway

import (
	"context"
	"time"

	"chainmaker.org/chainmaker/tcip-go/v2/api"
	"chainmaker.org/chainmaker/tcip-go/v2/common/cross_chain"
	"chainmaker.org/chainmaker/tcip-go/v2/common/relay_chain"
	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/metadata"
)

// RelayGateway 中继网关
type RelayGateway interface {
	CrossChainEvent(
		txRequest *cross_chain.CrossChainEventRequest) (*cross_chain.CrossChainEventResponse, error)
	QueryCrossChain(
		txRequest *relay_chain.QueryCrossChainRequest) (*relay_chain.QueryCrossChainResponse, error)
	BeginCrossChain(req *relay_chain.BeginCrossChainRequest) (*relay_chain.BeginCrossChainResponse, error)
	SyncBlockHeader(req *relay_chain.SyncBlockHeaderRequest) (*relay_chain.SyncBlockHeaderResponse, error)
}

type relayGateway struct {
	config *request.ClientConfig
	conn   *grpc.ClientConn
}

// CrossChainEvent  跨链事件操作
//
//	@receiver r
//	@param txRequest
//	@return *cross_chain.CrossChainEventResponse
//	@return error
func (r *relayGateway) CrossChainEvent(
	txRequest *cross_chain.CrossChainEventRequest) (*cross_chain.CrossChainEventResponse, error) {
	var err error
	if r.conn.GetState() != connectivity.Ready {
		r.conn, err = request.InitGRPCConnect(r.config)
		if err != nil {
			return nil, err
		}
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(r.config.Timeout)*time.Second)
	defer cancel()

	md := metadata.Pairs("x-token", r.config.AccessCode)
	metadataCtx := metadata.NewOutgoingContext(ctx, md)

	response, err := api.NewRpcRelayChainClient(r.conn).CrossChainEvent(metadataCtx, txRequest)
	return response, err
}

// QueryCrossChain  查询跨链
//
//	@receiver r
//	@param txRequest
//	@return *relay_chain.QueryCrossChainResponse
//	@return error
func (r *relayGateway) QueryCrossChain(
	txRequest *relay_chain.QueryCrossChainRequest) (*relay_chain.QueryCrossChainResponse, error) {
	var err error
	if r.conn.GetState() != connectivity.Ready {
		r.conn, err = request.InitGRPCConnect(r.config)
		if err != nil {
			return nil, err
		}
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(r.config.Timeout)*time.Second)
	defer cancel()

	md := metadata.Pairs("x-token", r.config.AccessCode)
	metadataCtx := metadata.NewOutgoingContext(ctx, md)

	response, err := api.NewRpcRelayChainClient(r.conn).QueryCrossChain(metadataCtx, txRequest)
	return response, err
}

// BeginCrossChain  开始跨链
//
//	@receiver r
//	@param req
//	@return *relay_chain.BeginCrossChainResponse
//	@return error
func (r *relayGateway) BeginCrossChain(
	req *relay_chain.BeginCrossChainRequest) (*relay_chain.BeginCrossChainResponse, error) {
	var err error
	if r.conn.GetState() != connectivity.Ready {
		r.conn, err = request.InitGRPCConnect(r.config)
		if err != nil {
			return nil, err
		}
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(r.config.Timeout)*time.Second)
	defer cancel()

	md := metadata.Pairs("x-token", r.config.AccessCode)
	metadataCtx := metadata.NewOutgoingContext(ctx, md)

	response, err := api.NewRpcRelayChainClient(r.conn).BeginCrossChain(metadataCtx, req)
	return response, err
}

// SyncBlockHeader  同步区块头
//
//	@receiver r
//	@param req
//	@return *relay_chain.SyncBlockHeaderResponse
//	@return error
func (r *relayGateway) SyncBlockHeader(
	req *relay_chain.SyncBlockHeaderRequest) (*relay_chain.SyncBlockHeaderResponse, error) {
	var err error
	if r.conn.GetState() != connectivity.Ready {
		r.conn, err = request.InitGRPCConnect(r.config)
		if err != nil {
			return nil, err
		}
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(r.config.Timeout)*time.Second)
	defer cancel()

	md := metadata.Pairs("x-token", r.config.AccessCode)
	metadataCtx := metadata.NewOutgoingContext(ctx, md)

	response, err := api.NewRpcRelayChainClient(r.conn).SyncBlockHeader(metadataCtx, req)
	return response, err
}

// NewRelayGateway  新建中继网关请求
//
//	@param config
//	@return RelayGateway
//	@return error
func NewRelayGateway(config *request.ClientConfig) (RelayGateway, error) {
	conn, err := request.InitGRPCConnect(config)
	if err != nil {
		return nil, err
	}
	return &relayGateway{
		config: config,
		conn:   conn,
	}, nil
}
