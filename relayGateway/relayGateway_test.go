package relayGateway

import (
	"testing"

	"chainmaker.org/chainmaker/tcip-go/v2/common/cross_chain"

	"chainmaker.org/chainmaker/tcip-go/v2/common/relay_chain"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
)

func TestSyncBlockHeader(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockConfig := &request.ClientConfig{
		// 添加必要的配置信息
	}

	mockReq := &relay_chain.SyncBlockHeaderRequest{
		// 添加必要的请求信息
	}

	t.Run("successful request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.SyncBlockHeader(mockReq)

		assert.NotNil(t, err)
	})

	t.Run("failed request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.SyncBlockHeader(mockReq)

		assert.Error(t, err, "期望有错误，但实际为 nil")
	})
}

func TestCrossChainEvent(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockConfig := &request.ClientConfig{
		// 添加必要的配置信息
	}

	mockReq := &cross_chain.CrossChainEventRequest{
		// 添加必要的请求信息
	}

	t.Run("successful request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.CrossChainEvent(mockReq)

		assert.NotNil(t, err)
	})

	t.Run("failed request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.CrossChainEvent(mockReq)

		assert.Error(t, err, "期望有错误，但实际为 nil")
	})
}

func TestQueryCrossChain(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockConfig := &request.ClientConfig{
		// 添加必要的配置信息
	}

	mockReq := &relay_chain.QueryCrossChainRequest{
		// 添加必要的请求信息
	}

	t.Run("successful request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.QueryCrossChain(mockReq)

		assert.NotNil(t, err)
	})

	t.Run("failed request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.QueryCrossChain(mockReq)

		assert.Error(t, err, "期望有错误，但实际为 nil")
	})
}

func TestBeginCrossChain(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockConfig := &request.ClientConfig{
		// 添加必要的配置信息
	}

	mockReq := &relay_chain.BeginCrossChainRequest{
		// 添加必要的请求信息
	}

	t.Run("successful request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.BeginCrossChain(mockReq)

		assert.NotNil(t, err)
	})

	t.Run("failed request", func(t *testing.T) {
		gateway, _ := NewRelayGateway(mockConfig)
		_, err := gateway.BeginCrossChain(mockReq)

		assert.Error(t, err, "期望有错误，但实际为 nil")
	})
}

func TestNewRelayGateway(t *testing.T) {
	mockConfig := &request.ClientConfig{
		// 添加必要的配置信息
	}

	t.Run("successful create gateway", func(t *testing.T) {
		_, err := NewRelayGateway(mockConfig)
		assert.Nil(t, err, "期望的错误为空，但实际错误为", err)
	})

	t.Run("failed to create gateway", func(t *testing.T) {
		// 在这里指定无效的配置信息
		_, err := NewRelayGateway(&request.ClientConfig{})
		assert.Nil(t, err)
	})
}
