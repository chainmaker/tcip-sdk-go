package crossChainGateway

import (
	"testing"

	"chainmaker.org/chainmaker/tcip-go/v2/common/cross_chain"
	"github.com/golang/mock/gomock"

	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
	"github.com/stretchr/testify/assert"
)

func testInit() {

}

func Test_crossChainGateway_NewCrossChainGateway(t *testing.T) {
	mockConfig := &request.ClientConfig{
		// Add necessary configurations here
	}

	t.Run("successful create gateway", func(t *testing.T) {
		_, err := NewCrossChainGateway(mockConfig)
		assert.Nil(t, err, "Expected nil error, but got", err)
	})

	t.Run("failed to create gateway", func(t *testing.T) {
		// Specify invalid configuration here
		_, err := NewCrossChainGateway(&request.ClientConfig{})
		assert.Nil(t, err, "Expected nil error, but got", err)
	})
}

func TestCrossChainEvent(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockConfig := &request.ClientConfig{
		// Add necessary configurations here
	}

	mockTxRequest := &cross_chain.CrossChainEventRequest{
		// Add necessary request details here
	}

	t.Run("successful request", func(t *testing.T) {
		gateway, _ := NewCrossChainGateway(mockConfig)
		_, err := gateway.CrossChainEvent(mockTxRequest)

		assert.NotNil(t, err)
	})

	t.Run("failed request", func(t *testing.T) {
		gateway, _ := NewCrossChainGateway(mockConfig)
		_, err := gateway.CrossChainEvent(mockTxRequest)

		assert.Error(t, err, "Expected error, but got nil")
	})
}
