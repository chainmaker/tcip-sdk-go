/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

// Package crossChainGateway 跨链网关
package crossChainGateway

import (
	"context"
	"time"

	"chainmaker.org/chainmaker/tcip-go/v2/api"
	"chainmaker.org/chainmaker/tcip-go/v2/common/cross_chain"
	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/metadata"
)

// CrossChainGateway 跨链网关
type CrossChainGateway interface {
	CrossChainEvent(
		txRequest *cross_chain.CrossChainEventRequest) (*cross_chain.CrossChainEventResponse, error)
}

type crossChainGateway struct {
	config *request.ClientConfig
	conn   *grpc.ClientConn
}

// CrossChainEvent  跨链事件操作
//
//	@receiver c
//	@param txRequest
//	@return *cross_chain.CrossChainEventResponse
//	@return error
func (c *crossChainGateway) CrossChainEvent(
	txRequest *cross_chain.CrossChainEventRequest) (*cross_chain.CrossChainEventResponse, error) {
	var err error
	if c.conn.GetState() != connectivity.Ready {
		c.conn, err = request.InitGRPCConnect(c.config)
		if err != nil {
			return nil, err
		}
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.config.Timeout)*time.Second)
	defer cancel()

	md := metadata.Pairs("x-token", c.config.AccessCode)
	metadataCtx := metadata.NewOutgoingContext(ctx, md)

	response, err := api.NewRpcCrossChainClient(c.conn).CrossChainEvent(metadataCtx, txRequest)
	return response, err
}

// NewCrossChainGateway  新建跨链网关请求
//
//	@param config
//	@return CrossChainGateway
//	@return error
func NewCrossChainGateway(config *request.ClientConfig) (CrossChainGateway, error) {
	conn, err := request.InitGRPCConnect(config)
	if err != nil {
		return nil, err
	}
	return &crossChainGateway{
		config: config,
		conn:   conn,
	}, nil
}
