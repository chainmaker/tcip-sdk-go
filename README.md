# TCIP go sdk

用来调用中继网关和跨链网关

## 软件环境依赖

golang ： 版本为1.20或以上

下载地址：https://golang.org/dl/

若已安装，请通过命令查看版本：

```shell
$ go version
go version go1.16 linux/amd64
```

## 下载安装sdk

进入您的Go项目，执行以下命令添加对sdk的引用：

```shell
go get chainmaker.org/chainmaker/tcip-sdk-go/v2
```

## 使用方法

```go
import (
	"chainmaker.org/chainmaker/common/v2/log"
	"chainmaker.org/chainmaker/tcip-go/v2/common/relay_chain"
	"chainmaker.org/chainmaker/tcip-go/v2/common/cross_chain"
	tcip_sdk_go "chainmaker.org/chainmaker/tcip-sdk-go/v2"
	"chainmaker.org/chainmaker/tcip-sdk-go/v2/request"
)
// 初始化配置
// 跨链网关配置
crossChainGatewayConfig1 = &request.ClientConfig{
		Address:        "127.0.0.1:19996",           // 跨链网关地址
		TlsEnable:      true,                        // 是否开启tls
		CaPath:         "./certs/ca_cross.crt",      // 跨链网关ca证书路径
		ClientCertPath: "./certs/client_cross.crt",  // 跨链网关客户端证书路径
		ClientKeyPath:  "./certs/client_cross.key",  // 跨链网关客户端私钥路径
		Timeout:        1000,                        // 超时时间
		ServerName:     "chainmaker.org",            // server name，取 chainmaker.org
		AccessCode:     "testAccessCode",            // 授权码，跨链网关不需要设置
}
// 中继网关配置
relayGatewayConfig = &request.ClientConfig{
		Address:        "127.0.0.1:19999",           // 中继网关地址
		TlsEnable:      true,                        // 是否开启tls
		CaPath:         "./certs/ca_relay.crt",      // 中继网关ca证书路径
		ClientCertPath: "./certs/client_relay.crt",  // 中继网关客户端证书路径
		ClientKeyPath:  "./certs/client_relay.key",  // 中继网关客户端私钥路径
		Timeout:        1000,                        // 超时时间
		ServerName:     "chainmaker.org",            // server name，取 chainmaker.org
		AccessCode:     "testAccessCode",            // 授权码，申请加入主链审批通过后，在页面上获取
}
// 初始化sdk
func initTcipSdk() {
  // 初始化日志
  logger, _ := log.InitSugarLogger(&log.LogConfig{
		Module:       "[TCIP-SDK]",                                 // 日志名称
		LogPath:      path.Join(os.TempDir(), time.Now().String()), // 日志存储位置
		LogLevel:     log.LEVEL_DEBUG,                              // 日志等级
		MaxAge:       300,                                          // 最大保留天数
		JsonFormat:   false,                                        // 是否使用json格式
		ShowLine:     true,                                         // 显示文件名和行号
		LogInConsole: true,                                         // 是否打印到标准输出
		RotationSize: 10,                                           // 日志切换大小
	})
  // 设置日志属性
  crossChainGatewayConfig1.Log = logger
	relayGatewayConfig.Log = logger
	// 初始化sdk
  tcipSdk, err := tcip_sdk_go.NewTcipSdk(crossChainGatewayConfig1, relayGatewayConfig)
	if err != nil {
		panic(err)
	}
  // 获取跨链网关对象
  crossChainGateway1 := tcipSdk.CrossChainGateway
  // 获取中继网关对象
	relayGateway := tcipSdk.RelayGateway
  // 跨链网关跨链事件触发配置，参数以及返回值详细信息参考参数结构体注释（可以使用链sdk直接调用跨链合约设置）
  // 该方法只适用于示例跨链网关，不适用自行实现的跨链网关
  crossChainGateway1.CrossChainEvent(&cross_chain.CrossChainEventRequest{})
  // 中继网关跨链事件触发配置，参数以及返回值详细信息参考参数结构体注释（可以使用长安链sdk直接调用跨链合约设置）
  relayGateway.CrossChainEvent(&cross_chain.CrossChainEventRequest{})
  // 通过中继网关查询跨链信息，参数以及返回值详细信息参考参数结构体注释（可以使用长安链sdk直接调用RELAY_CROSS合约直接查询）
  relayGateway.QueryCrossChain(&relay_chain.QueryCrossChainRequest{})
  // 向中继链发起跨链请求，参数以及返回值详细信息参考参数结构体注释
  relayGateway.BeginCrossChain(&relay_chain.BeginCrossChainRequest{})
 	// 向中继链发起同步区块头请求，参数以及返回值详细信息参考结构体注释
  relayGateway.SyncBlockHeader(&relay_chain.SyncBlockHeaderRequest{})
}
```

